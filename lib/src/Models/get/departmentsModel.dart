// To parse this JSON data, do
//
//     final departmentsModel = departmentsModelFromJson(jsonString);

import 'dart:convert';

DepartmentsModel departmentsModelFromJson(String str) => DepartmentsModel.fromJson(json.decode(str));

String departmentsModelToJson(DepartmentsModel data) => json.encode(data.toJson());

class DepartmentsModel {
    DepartmentsModel({
        this.data,
        this.status,
        this.message,
    });

    List<Department> data;
    String status;
    String message;

    factory DepartmentsModel.fromJson(Map<String, dynamic> json) => DepartmentsModel(
        data: json["data"] == null ? null : List<Department>.from(json["data"].map((x) => Department.fromJson(x))),
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status == null ? null : status,
        "message": message == null ? null : message,
    };
}

class Department {
    Department({
        this.id,
        this.key,
        this.name,
        this.imageurl,
        this.imageurlorg,
    });

    int id;
    String key;
    String name;
    String imageurl;
    String imageurlorg;

    factory Department.fromJson(Map<String, dynamic> json) => Department(
        id: json["id"] == null ? null : json["id"],
        key: json["key"] == null ? null : json["key"],
        name: json["name"] == null ? null : json["name"],
        imageurl: json["imageurl"] == null ? null : json["imageurl"],
        imageurlorg: json["imageurlorg"] == null ? null : json["imageurlorg"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "key": key == null ? null : key,
        "name": name == null ? null : name,
        "imageurl": imageurl == null ? null : imageurl,
        "imageurlorg": imageurlorg == null ? null : imageurlorg,
    };
}
