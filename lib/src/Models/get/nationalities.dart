// To parse this JSON data, do
//
//     final nationalities = nationalitiesFromJson(jsonString);

import 'dart:convert';

Nationalities nationalitiesFromJson(String str) => Nationalities.fromJson(json.decode(str));

String nationalitiesToJson(Nationalities data) => json.encode(data.toJson());

class Nationalities {
    Nationalities({
        this.data,
        this.status,
        this.message,
    });

    List<Datum> data;
    String status;
    String message;

    factory Nationalities.fromJson(Map<String, dynamic> json) => Nationalities(
        data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status == null ? null : status,
        "message": message == null ? null : message,
    };
}

class Datum {
    Datum({
        this.id,
        this.name,
    });

    int id;
    String name;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
    };
}
