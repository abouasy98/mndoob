// To parse this JSON data, do
//
//     final authModel = authModelFromJson(jsonString);

import 'dart:convert';

AuthModel authModelFromJson(String str) => AuthModel.fromJson(json.decode(str));

String authModelToJson(AuthModel data) => json.encode(data.toJson());

class AuthModel {
    AuthModel({
        this.data,
        this.status,
        this.message,
    });

    UserData data;
    String status;
    String message;

    factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        data: json["data"] == null ? null : UserData.fromJson(json["data"]),
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "status": status == null ? null : status,
        "message": message == null ? null : message,
    };
}

class UserData {
    UserData({
        this.email,
        this.mobile,
        this.fullName,
        this.lang,
        this.active,
        this.code,
        this.cityId,
        this.type,
        this.id,
        this.jwt,
        this.online,
        this.imageurl,
        this.imageurlorg,
        this.dashboardurl,
    });

    String email;
    String mobile;
    String fullName;
    String lang;
    String active;
    String code;
    String cityId;
    String type;
    int id;
    String jwt;
    String online;
    String imageurl;
    String imageurlorg;
    String dashboardurl;

    factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        email: json["email"] == null ? null : json["email"],
        mobile: json["mobile"] == null ? null : json["mobile"],
        fullName: json["full_name"] == null ? null : json["full_name"],
        lang: json["lang"] == null ? null : json["lang"],
        active: json["active"] == null ? null : json["active"],
        code: json["code"] == null ? null : json["code"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        type: json["type"] == null ? null : json["type"],
        id: json["id"] == null ? null : json["id"],
        jwt: json["jwt"] == null ? null : json["jwt"],
        online: json["online"] == null ? null : json["online"],
        imageurl: json["imageurl"] == null ? null : json["imageurl"],
        imageurlorg: json["imageurlorg"] == null ? null : json["imageurlorg"],
        dashboardurl: json["dashboardurl"] == null ? null : json["dashboardurl"],
    );

    Map<String, dynamic> toJson() => {
        "email": email == null ? null : email,
        "mobile": mobile == null ? null : mobile,
        "full_name": fullName == null ? null : fullName,
        "lang": lang == null ? null : lang,
        "active": active == null ? null : active,
        "code": code == null ? null : code,
        "city_id": cityId == null ? null : cityId,
        "type": type == null ? null : type,
        "id": id == null ? null : id,
        "jwt": jwt == null ? null : jwt,
        "online": online == null ? null : online,
        "imageurl": imageurl == null ? null : imageurl,
        "imageurlorg": imageurlorg == null ? null : imageurlorg,
        "dashboardurl": dashboardurl == null ? null : dashboardurl,
    };
}
