// To parse this JSON data, do
//
//     final registerMobileModel = registerMobileModelFromJson(jsonString);

import 'dart:convert';

RegisterMobileModel registerMobileModelFromJson(String str) => RegisterMobileModel.fromJson(json.decode(str));

String registerMobileModelToJson(RegisterMobileModel data) => json.encode(data.toJson());

class RegisterMobileModel {
    RegisterMobileModel({
        this.data,
        this.status,
        this.message,
    });

    Data data;
    String status;
    String message;

    factory RegisterMobileModel.fromJson(Map<String, dynamic> json) => RegisterMobileModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "status": status == null ? null : status,
        "message": message == null ? null : message,
    };
}

class Data {
    Data({
        this.code,
    });

    int code;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        code: json["code"] == null ? null : json["code"],
    );

    Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
    };
}
