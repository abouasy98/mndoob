// To parse this JSON data, do
//
//     final termsandconditionModel = termsandconditionModelFromJson(jsonString);

import 'dart:convert';

TermsandconditionModel termsandconditionModelFromJson(String str) => TermsandconditionModel.fromJson(json.decode(str));

String termsandconditionModelToJson(TermsandconditionModel data) => json.encode(data.toJson());

class TermsandconditionModel {
    TermsandconditionModel({
        this.data,
        this.status,
        this.message,
    });

    Data data;
    String status;
    String message;

    factory TermsandconditionModel.fromJson(Map<String, dynamic> json) => TermsandconditionModel(
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "status": status == null ? null : status,
        "message": message == null ? null : message,
    };
}

class Data {
    Data({
        this.pageName,
        this.content,
    });

    String pageName;
    String content;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        pageName: json["page_name"] == null ? null : json["page_name"],
        content: json["content"] == null ? null : json["content"],
    );

    Map<String, dynamic> toJson() => {
        "page_name": pageName == null ? null : pageName,
        "content": content == null ? null : content,
    };
}
