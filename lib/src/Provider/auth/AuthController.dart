import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mndoob/src/Helpers/map_helper.dart';
import 'package:mndoob/src/MainWidgets/custom_alert.dart';
import 'package:mndoob/src/MainWidgets/custom_new_dialog.dart';
import 'package:mndoob/src/MainWidgets/custom_progress_dialog.dart';
import 'package:mndoob/src/Models/auth/AuthModle.dart';
import 'package:mndoob/src/Models/auth/editPassModel.dart';
import 'package:mndoob/src/Models/auth/phoneVerificationModel.dart';
import 'package:mndoob/src/Models/auth/register_mobileModel.dart';
import 'package:mndoob/src/Repository/networkUtlis.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AuthController extends ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  RegisterMobileModel _registerMobileModel;
  PhoneVerificationModel _phoneVerificationModel;
  AuthModel userModel;
  SharedPreferences _prefs;
  CustomAlert _alert = CustomAlert();
  String type;
  String name;
  String details;
  List<File> imgs;
  List<Asset> photos;
  File photo;
  String cityId;
  int carType;
  String phone;

  Map<String, String> headers = {"X-localization": "ar"};
  FirebaseMessaging _fcm = FirebaseMessaging();

  Future<bool> registerMobile(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();

    FormData _formData = FormData.fromMap({
      "mobile": phone,
    });
    Response response =
        await _utils.post("auth/ask_reset", headerss: headers, body: _formData);

    if (response.statusCode == 200) {
      print("sucss");
      _registerMobileModel = RegisterMobileModel.fromJson(response.data);

      print("sendCode Sucss");
      customProgressDialog.hidePr();
      _alert.toast(context: context, title: _registerMobileModel.message);

      notifyListeners();
      return true;
    } else {
      print("sendCode error");
      _registerMobileModel = RegisterMobileModel.fromJson(response.data);
      customProgressDialog.hidePr();
      _alert.toast(context: context, title: _registerMobileModel.message);
      notifyListeners();
      return false;
    }
  }

  Future<bool> phoneVerification(BuildContext context, String code) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();

    FormData _formData = FormData.fromMap({
      "phone_number": phone,
      "country_code": "02",
      "code": code,
    });
    Response response = await _utils.post("phone-verification",
        headerss: headers, body: _formData);

    if (response.statusCode == 200) {
      print("sucss");
      _phoneVerificationModel = PhoneVerificationModel.fromJson(response.data);

      print("get Sucss");
      customProgressDialog.hidePr();
      _alert.toast(context: context, title: _phoneVerificationModel.data.value);
      notifyListeners();
      return true;
    } else {
      print("sendCode error");
      _phoneVerificationModel = PhoneVerificationModel.fromJson(response.data);
      customProgressDialog.hidePr();
      _alert.toast(
          context: context, title: _phoneVerificationModel.error.first.value);
      notifyListeners();
      return false;
    }
  }

  Future<bool> resendCode(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();

    FormData _formData = FormData.fromMap({
      "phone_number": phone,
      "country_code": "02",
    });
    Response response =
        await _utils.post("resend-code", headerss: headers, body: _formData);

    if (response.statusCode == 200) {
      print("sucss");
      _registerMobileModel = RegisterMobileModel.fromJson(response.data);

      print("sendCode Sucss");
      customProgressDialog.hidePr();
      _alert.toast(context: context, title: _registerMobileModel.message);
      notifyListeners();
      return true;
    } else {
      print("sendCode error");
      _registerMobileModel = RegisterMobileModel.fromJson(response.data);
      customProgressDialog.hidePr();
      _alert.toast(context: context, title: _registerMobileModel.message);
      notifyListeners();
      return false;
    }
  }

  Future<bool> register(
    BuildContext context, {
    String name,
    String email,
    String password,
    String passwordConfirm,
    String countryCode,
    File photo,
    String cityId,
    File identity,
    File license,
    File carHeadImage,
    File carBehingImage,
    File carForm,
    int nationalId,
  }) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    _prefs = await SharedPreferences.getInstance();
    String deviceToken = await _fcm.getToken();
    print("deviceToken : $deviceToken");
    FormData _formData = FormData.fromMap({
      "full_name": name,
      "email": email,
      "password": password,
      "confirmation": passwordConfirm,
      "mobile": phone,
      "image": photo == null ? null : await MultipartFile.fromFile(photo.path),
      "city_id": cityId,
      "type": type,
      "national_id": nationalId,
      "nationality_id": countryCode,
      "card_id_image":
          identity == null ? null : await MultipartFile.fromFile(identity.path),
      "driving_license_image":
          license == null ? null : await MultipartFile.fromFile(license.path),
      "car_form":
          carForm == null ? null : await MultipartFile.fromFile(carForm.path),
      "car_head_image": carHeadImage == null
          ? null
          : await MultipartFile.fromFile(carHeadImage.path),
      "car_behind_image": carBehingImage == null
          ? null
          : await MultipartFile.fromFile(carBehingImage.path),
    });

    Response response =
        await _utils.post("signup", headerss: headers, body: _formData);

    if (response.statusCode == 200) {
      print("register succ");
      userModel = AuthModel.fromJson(response.data);
      type = userModel.data.type;
      userModel.data.active = userModel.data.active;
      Provider.of<AuthController>(context, listen: false).phone = phone;
      NetworkUtil.token = userModel.data.jwt;
      storageUserData(json.encode(response.data));
      customProgressDialog.hidePr();
      notifyListeners();
      return true;
    } else {
      print("error");
      userModel = AuthModel.fromJson(response.data);
      _alert.toast(context: context, title: userModel.message);
      customProgressDialog.hidePr();
      notifyListeners();
      return false;
    }
  }

  Future<bool> login(BuildContext context, {String password}) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    String deviceToken = await _fcm.getToken();
    print(deviceToken);
    FormData _formData = FormData.fromMap({
      "phone_number": phone,
      "country_code": "20",
      "device_token": deviceToken,
      "password": password,
    });
    Response response = await _utils.post("login", body: _formData);
    if (response.statusCode == 200) {
      print("register succ");
      userModel = AuthModel.fromJson(response.data);
      NetworkUtil.token = userModel.data.jwt;
      type = userModel.data.type;
      storageUserData(json.encode(response.data));
      customProgressDialog.hidePr();
      notifyListeners();
      return true;
    } else {
      print("error");
      userModel = AuthModel.fromJson(response.data);
      _alert.toast(context: context, title: userModel.message);
      customProgressDialog.hidePr();
      notifyListeners();
      return false;
    }
  }

  storageUserData(String data) async {
    _prefs = await SharedPreferences.getInstance();
    _prefs.setString("userData", data);
  }

  Future<bool> getStorgeData() async {
    _prefs = await SharedPreferences.getInstance();
    String data = _prefs.getString("userData");
    print(data);
    if (data != null) {
      userModel = AuthModel.fromJson(json.decode(data));
      NetworkUtil.token = userModel.data.jwt;
      type = userModel.data.type;
      name = userModel.data.fullName;

      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  editUserData(
    BuildContext context,
  ) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    AuthModel edit;
    List<MultipartFile> _photos = [];
    if (photos != null && photos.length > 0) {
      for (int i = 0; i < photos.length; i++) {
        ByteData byteData = await photos[i].getByteData();
        List<int> imageData = byteData.buffer.asUint8List();
        MultipartFile multipartFile = MultipartFile.fromBytes(imageData,
            filename: '${photos[i].toString()}.jpg');
        _photos.add(multipartFile);
      }
    }
    print("hi image");

    // for (int i = 0; i < imgs.length; i++) {
    //   print("hi image");
    //   _photos.add(await MultipartFile.fromFile(imgs[i].path));
    // }
    FormData _formData = FormData.fromMap({
      "name": name,
      "photo": photo == null ? null : await MultipartFile.fromFile(photo.path),
      "city_id": cityId,
      "car_type": carType,
      "photos": _photos,
      "details": details,
    });

    Response response = await _utils.post("profile-edit", body: _formData);

    if (response.statusCode == 200) {
      print("edit succ");
      edit = AuthModel.fromJson(response.data);
      userModel.data.fullName = edit.data.fullName;
      userModel.data.imageurl = edit.data.imageurl;
      userModel.data.cityId = edit.data.cityId;
      userModel.data.imageurlorg = edit.data.imageurlorg;
      userModel.data.active = edit.data.active;
      userModel.data.email = edit.data.email;
      userModel.data.id = edit.data.id;
      userModel.data.mobile = edit.data.mobile;

      storageUserData(json.encode(userModel.toJson()));
      customProgressDialog.hidePr();
      notifyListeners();
      return true;
    } else {
      print("error");
      userModel = AuthModel.fromJson(response.data);
      _alert.toast(context: context, title: userModel.message);
      customProgressDialog.hidePr();
      notifyListeners();
      return false;
    }
  }

  editPassword(BuildContext context,
      {String oldPass, String newPass, String regPass}) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    EditPasswordModel _model;
    FormData _formData = FormData.fromMap({
      "current_password": oldPass,
      "new_password": newPass,
      "password_confirmation": regPass,
    });

    Response response = await _utils.post("change-password", body: _formData);

    if (response.statusCode == 200) {
      print("Edit sucss");
      _model = EditPasswordModel.fromJson(response.data);
      customProgressDialog.hidePr();
      _alert.toast(context: context, title: _model.data.value);
      notifyListeners();
    } else {
      _model = EditPasswordModel.fromJson(response.data);
      customProgressDialog.hidePr();
      _alert.toast(context: context, title: _model.error.first.value);
      notifyListeners();
    }
  }

  editCarData(
      BuildContext context, File identity, File license, File carForm) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    FormData _formData = FormData.fromMap({
      "car_form":
          carForm == null ? null : await MultipartFile.fromFile(carForm.path),
      "identity":
          identity == null ? null : await MultipartFile.fromFile(identity.path),
      "license":
          license == null ? null : await MultipartFile.fromFile(license.path),
    });
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Response response = await _utils.post("edit-car-data", body: _formData);
    if (response.statusCode == 200) {}
  }
}
