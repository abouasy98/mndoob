import 'package:mndoob/src/MainWidgets/customBtn.dart';
import 'package:mndoob/src/extentions/extentionsMethods.dart';
import 'package:mndoob/src/MainWidgets/image_bg2.dart';
import 'package:mndoob/src/MainWidgets/register_text_field.dart';
import 'package:mndoob/src/Provider/auth/forgetPasswordProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'widget/backBtn.dart';
import 'widget/lockIcon.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      body: Stack(
        children: [
          ImageBG2(),
          Form(
            key: _form,
            autovalidateMode:
                autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
            child: Stack(
              children: <Widget>[
                ListView(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Spacer(flex: 5),
                          Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 40.0, right: 8, left: 8),
                                child: Container(
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: context.height * 0.1,
                                      ),
                                      Center(
                                          child: Text(
                                        "فضلا",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 36,
                                            fontWeight: FontWeight.bold),
                                      )),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Center(
                                          child: Text(
                                        "ادخل رقم الجوال ليصلك كود التفعيل",
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 13),
                                        textAlign: TextAlign.center,
                                      )),
                                      SizedBox(
                                        height: context.height * 0.06,
                                      ),
                                      RegisterTextField(
                                        label: 'رقم الجوال',
                                        onChange: (v) {
                                          Provider.of<ForgetPasswordProvider>(
                                                  context,
                                                  listen: false)
                                              .phone = v;
                                        },
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      CustomBtn(
                                        txtColor: Colors.white,
                                        heigh: 50,
                                        onTap: () {
                                          setState(() {
                                            autoError = true;
                                          });
                                          final isValid =
                                              _form.currentState.validate();
                                          if (!isValid) {
                                            return;
                                          }
                                          _form.currentState.save();
                                          Provider.of<ForgetPasswordProvider>(
                                                  context,
                                                  listen: false)
                                              .forgetPassword(context);
                                        },
                                        color: Theme.of(context).primaryColor,
                                        text: 'ارسال',
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                  left: 0,
                                  right: 0,
                                  top: context.height * 0.01,
                                  child: Logo()),
                            ],
                          ),
                          Spacer(flex: 2)
                        ],
                      ),
                    ),
                  ],
                ),
                Positioned(left: 10, top: 40, child: BackBtn())
              ],
            ),
          ),
        ],
      ),
    );
  }
}
