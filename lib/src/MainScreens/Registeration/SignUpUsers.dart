import 'package:location/location.dart';
import 'package:mndoob/src/Helpers/map_helper.dart';
import 'package:mndoob/src/Helpers/route.dart';
import 'package:mndoob/src/MainScreens/Registeration/registerMobilePage.dart';
// import 'package:shobek/src/MainScreens/shop/ShopMainPage/ShopMainPage.dart';
import 'package:mndoob/src/MainWidgets/customBtn.dart';
import 'package:mndoob/src/MainWidgets/custom_alert.dart';
import 'package:mndoob/src/MainWidgets/image_bg.dart';
import 'package:mndoob/src/MainWidgets/image_bg2.dart';
import 'package:mndoob/src/MainWidgets/labeled_bottom_sheet.dart';
import 'package:mndoob/src/MainWidgets/register_secure_text_field.dart';
import 'package:mndoob/src/MainWidgets/register_text_field.dart';
import 'package:mndoob/src/MainWidgets/terms_dialog.dart';
import 'package:mndoob/src/Provider/auth/AuthController.dart';
import 'package:mndoob/src/Provider/get/RegisterHelper.dart';
import 'package:mndoob/src/Provider/termsProvider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../mainPage.dart';
import 'confirmCode.dart';
import 'widget/appIcon.dart';
import 'widget/backBtn.dart';
import 'widget/lockIcon.dart';

class SignUpUsers extends StatefulWidget {
  @override
  _SignUpUsersState createState() => _SignUpUsersState();
}

class _SignUpUsersState extends State<SignUpUsers> {
  bool _accept = false;
  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    Provider.of<MapHelper>(context, listen: false).getLocation();
    Provider.of<TermsProvider>(context, listen: false).getTerms();
    Provider.of<RegisterHelper>(context, listen: false).getCountries(context);
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });
    super.initState();
  }

  final mobileController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController passwordConfirm = TextEditingController();

  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      key: _globalKey,
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: [
          ImageBG2(
            height: MediaQuery.of(context).size.height * 0.45,
          ),
          SingleChildScrollView(
            child: Form(
              key: _form,
              autovalidateMode: autoError
                  ? AutovalidateMode.always
                  : AutovalidateMode.disabled,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 40,
                      left: 10,
                    ),
                    child: BackBtn(
                      color: Colors.white,
                    ),
                  ),
                  Column(
                    children: [
                      Logo(),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          "تسجيل جديد كعميل",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 15),
                        ),
                      ),
                      SizedBox(height: 30),
                      RegisterTextField(
                        controller: nameController,
                        // icon: Icons.person,
                        onChange: (v) {},
                        label: 'الاسم',
                        hint: "اكتب اسم العميل",
                        type: TextInputType.text,
                        leghth: 20,
                      ),
                      SizedBox(height: 30),
                      RegisterTextField(
                        controller: mobileController,
                        label: 'رقم الجوال',
                      ),
                      SizedBox(height: 10),
                      SizedBox(height: 30),
                      RegisterTextField(
                        controller: emailController,
                        label: 'البريد الالكتروني',
                      ),
                      SizedBox(height: 10),
                      LabeledBottomSheet(
                        label: "اختر الدولة",
                        onChange: (v) {
                          print("$v");
                        },
                        data: Provider.of<RegisterHelper>(context, listen: true)
                            .nationalities,
                      ),
                      SizedBox(height: 20),
                      RegisterSecureTextField(
                        controller: password,
                        onChange: (v) {},
                        label: "كلمة المرور",
                        // icon: Icons.lock,
                      ),
                      SizedBox(height: 20),
                      RegisterSecureTextField(
                        controller: passwordConfirm,
                        onChange: (v) {},
                        // icon: Icons.lock,
                        label: 'تأكيد كلمة المرور',
                      ),
                      SizedBox(height: 20),
                      Theme(
                        data: Theme.of(context)
                            .copyWith(unselectedWidgetColor: Colors.white),
                        child: CheckboxListTile(
                          value: _accept,
                          onChanged: (value) {
                            setState(() {
                              _accept = !_accept;
                            });
                          },
                          checkColor: Theme.of(context).primaryColor,
                          activeColor: Color(0xff414141),
                          dense: true,
                          title: InkWell(
                            onTap: () => TermsDialog().show(context: context),
                            child: Text(
                              'اوافق على الشروط والأحكام',
                              textAlign: TextAlign.right,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 5,
                        ),
                        child: CustomBtn(
                          txtColor: Colors.white,
                          heigh: 50,
                          onTap: () async {
                            setState(() {
                              autoError = true;
                            });
                            // final isValid =
                            //     _form.currentState.validate();
                            // if (!isValid) {
                            //   return;
                            // }
                            _form.currentState.save();

                            if (_accept == false) {
                              CustomAlert().toast(
                                context: context,
                                title: 'يجب الموافقة على الشروط والأحكام',
                              );
                            }
                            if (!await Location().serviceEnabled()) {
                              CustomAlert().toast(
                                  context: context,
                                  title: 'يجب تشغيل خاصية تجديد المواقع');
                              await Provider.of<MapHelper>(context,
                                      listen: false)
                                  .getLocation();
                            } else {
                              _form.currentState.save();

                              Provider.of<AuthController>(context,
                                      listen: false)
                                  .phone = mobileController.text;
                              Provider.of<AuthController>(context,
                                      listen: false)
                                  .register(
                                context,
                                email: emailController.text,
                                name: nameController.text,
                                password: password.text,
                                passwordConfirm: passwordConfirm.text,
                              )
                                  .then((value) {
                                if (value == true) {
                                  pushAndRemoveUntil(
                                      context,
                                      ConfirmCode(
                                        phoneNumber: mobileController.text,
                                        stateOfVerificationCode: 0,
                                      ));
                                }
                              });
                              // Navigator.of(context).push(
                              //   MaterialPageRoute(
                              //     builder: (_) => MainPage(),
                              //   ),
                              // );
                            }
                          },
                          color: Theme.of(context).primaryColor,
                          text: 'تسجيل',
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
