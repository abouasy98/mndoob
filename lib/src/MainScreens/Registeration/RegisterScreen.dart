import 'package:provider/provider.dart';
import 'package:mndoob/src/Helpers/route.dart';
import 'package:mndoob/src/MainScreens/Registeration/registerMobilePage.dart';
import 'package:mndoob/src/MainWidgets/customBtn.dart';
import 'package:mndoob/src/MainWidgets/image_bg2.dart';
import 'package:flutter/material.dart';
import 'package:mndoob/src/Provider/auth/AuthController.dart';
import 'SignUpDriver.dart';
import 'SignUpShop.dart';
import 'SignUpUsers.dart';
import 'widget/appIcon.dart';
import 'widget/backBtn.dart';
import 'widget/lockIcon.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      body: Stack(
        children: [
          ImageBG2(),
          Stack(
            children: <Widget>[
              ListView(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Spacer(flex: 7),
                        Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 30.0,
                                //  bottom: 30.0,
                                left: 10,
                                right: 10,
                              ),
                              child: Column(
                                children: [
                                  Logo(),
                                  Center(
                                    child: Text(
                                      "اختر طريقة التسجيل",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(height: 40),
                                  // Register as user
                                  CustomBtn(
                                    color: Theme.of(context).primaryColor,
                                    txtColor: Colors.white,
                                    heigh: 60,
                                    text: 'التسجيل كعميل',
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    onTap: () {
                                      setState(() {
                                        Provider.of<AuthController>(context,
                                                listen: false)
                                            .type = "client";
                                      });
                                      push(context, SignUpUsers());
                                      // Navigator.of(context).push(
                                      //   MaterialPageRoute(
                                      //     builder: (_) => SignUpUsers(),
                                      //   ),
                                      // );
                                    },
                                  ),
                                  SizedBox(height: 5),
                                  // Register as Driver
                                  CustomBtn(
                                    color: Colors.black87,
                                    txtColor: Colors.white,
                                    heigh: 60,
                                    text: 'التسجيل كمندوب',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    onTap: () {
                                      setState(() {
                                        Provider.of<AuthController>(context,
                                                listen: false)
                                            .type = "driver";
                                      });
                                      push(context, SignUpDriver());
                                      // Navigator.of(context).push(
                                      //   MaterialPageRoute(
                                      //     builder: (_) => SignUpDriver(),
                                      //   ),
                                      // );
                                    },
                                  ),
                                  SizedBox(height: 5),
                                  // Register as shop
                                  CustomBtn(
                                    color: Colors.black,
                                    txtColor: Colors.white,
                                    heigh: 60,
                                    text: 'التسجيل كفني',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    onTap: () {
                                      setState(() {
                                        Provider.of<AuthController>(context,
                                                listen: false)
                                            .type = "tech";
                                      });
                                      push(context, SignUpShop());
                                      // Navigator.of(context).push(
                                      //   MaterialPageRoute(
                                      //     builder: (_) => SignUpShop(),
                                      //   ),
                                      // );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Spacer(flex: 2)
                      ],
                    ),
                  ),
                ],
              ),
              Positioned(
                  left: 10,
                  top: 40,
                  child: BackBtn(
                    color: Colors.white,
                  ))
            ],
          ),
        ],
      ),
    );
  }
}
