import 'package:mndoob/src/Helpers/route.dart';
import 'package:mndoob/src/MainScreens/driver/driverMainPage.dart';
import 'package:mndoob/src/MainScreens/mainPage.dart';
import 'package:mndoob/src/MainScreens/shop/ShopMainPage.dart';
import 'package:mndoob/src/MainWidgets/customBtn.dart';
import 'package:mndoob/src/MainWidgets/image_bg2.dart';
import 'package:mndoob/src/MainWidgets/register_secure_text_field.dart';
import 'package:mndoob/src/MainWidgets/register_text_field.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:mndoob/src/Provider/auth/AuthController.dart';
import '../../Provider/auth/loginProvider.dart';
import 'RegisterScreen.dart';
import 'forgotPasswordScreen.dart';
import 'widget/appIcon.dart';
import 'widget/lockIcon.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var loginProvider = Provider.of<LoginProvider>(context, listen: false);
    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        body: Stack(
          children: [
            ImageBG2(),
            Form(
                key: _form,
                autovalidateMode: autoError
                    ? AutovalidateMode.always
                    : AutovalidateMode.disabled,
                child: Stack(children: <Widget>[
                  ListView(
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.9,
                          child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Spacer(
                                  flex: 1,
                                ),
                                Logo(),
                                Padding(
                                    padding: const EdgeInsets.only(
                                        top: 20.0, right: 8, left: 8),
                                    child: Column(children: [
                                      RegisterTextField(
                                          controller: phoneController,
                                          label: 'رقم الجوال',
                                          onChange: (v) {
                                            loginProvider.phone = v;
                                          }),
                                      SizedBox(height: 20),
                                      RegisterSecureTextField(
                                          controller: passwordController,
                                          label: "كلمة المرور",
                                          onChange: (v) {
                                            loginProvider.password = v;
                                          }),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: TextButton(
                                            child: Text('نسيت كلمة المرور ؟',
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                )),
                                            onPressed: () {
                                              Navigator.of(context).push(
                                                MaterialPageRoute(
                                                  builder: (_) =>
                                                      ForgotPasswordScreen(),
                                                ),
                                              );
                                            }),
                                      ),
                                      CustomBtn(
                                        txtColor: Colors.white,
                                        heigh: 45,
                                        onTap: () {
                                          setState(() {
                                            autoError = true;
                                          });
                                          final isValid =
                                              _form.currentState.validate();
                                          if (!isValid) {
                                            return;
                                          }
                                          _form.currentState.save();
                                          Provider.of<AuthController>(context,
                                                  listen: false)
                                              .phone = phoneController.text;
                                          Provider.of<AuthController>(context,
                                                  listen: false)
                                              .login(
                                            context,
                                            password: passwordController.text,
                                          )
                                              .then((value) {
                                            if (value == true) {
                                              String type =
                                                  Provider.of<AuthController>(
                                                          context,
                                                          listen: false)
                                                      .type;
                                              if (type == "client") {
                                                pushAndRemoveUntil(
                                                    context, MainPage());
                                              } else if (type == "driver") {
                                                pushAndRemoveUntil(
                                                    context, DriverMainPage());
                                              } else {
                                                pushAndRemoveUntil(
                                                    context, ShopMainPage());
                                              }
                                            }
                                          });
                                        },
                                        color: Theme.of(context).primaryColor,
                                        text: 'تسجيل دخول',
                                      ),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Center(
                                                child: TextButton(
                                                    child: Text("اضغط هنا",
                                                        style: TextStyle(
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor)),
                                                    onPressed: () {
                                                      Navigator.of(context).push(
                                                          MaterialPageRoute(
                                                              builder: (_) =>
                                                                  RegisterScreen()));
                                                    })),
                                            Text('..ليس لديك حساب',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 15)),
                                          ])
                                    ])),
                              ]),
                        )
                      ]),
                ])),
          ],
        ));
  }
}
