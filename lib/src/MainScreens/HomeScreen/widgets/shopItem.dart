import 'package:flutter/material.dart';
import 'package:mndoob/src/MainWidgets/custom_alert.dart';
import 'package:mndoob/src/MainWidgets/diamond.dart';

class ShopItem extends StatelessWidget {
  final Function onTap;
  final String image, productQuantity, shopName, distance, view;
  final int open;
  const ShopItem({
    Key key,
    this.onTap,
    @required this.image,
    @required this.shopName,
    @required this.productQuantity,
    @required this.distance,
    @required this.view,
    @required this.open,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (onTap != null && open == 1)
          ? onTap
          : () {
              CustomAlert().toast(
                context: context,
                title: "المتجر غير متاح حاليا",
              );
            },
      child: Container(
        height: MediaQuery.of(context).size.height * 0.1,
        child: Stack(children: [
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.08,
              child: Card(
                color: open == 1 ? Colors.white : Colors.grey[400],
                elevation: 10,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 15,
                    right: MediaQuery.of(context).size.width * 0.13,
                    top: 5,
                  ),
                  child:
                      // Shop name and distance
                      Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // shop Name
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            shopName.length > 20
                                ? shopName.substring(0, 20) + ".."
                                : shopName,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            productQuantity.length > 20
                                ? productQuantity.substring(0, 20) + ".."
                                : productQuantity,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      // Location

                      Text(
                        "$distance كم",
                        style: TextStyle(
                          fontSize: 14,
                          letterSpacing: 1.5,
                          height: 2,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            right: 10,
            height: MediaQuery.of(context).size.height * 0.095,
            width: MediaQuery.of(context).size.width * 0.15,
            child: FloatingActionButton(
              backgroundColor: Colors.white,
              shape: DiamondBorder(),
              onPressed: () {},
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: FadeInImage.assetNetwork(
                  placeholder: "assets/images/image.jpg",
                  fadeInDuration: Duration(seconds: 2),
                  image: image,
                  height: MediaQuery.of(context).size.height * 0.055,
                  width: MediaQuery.of(context).size.width * 0.09,
                  fit: BoxFit.cover,
                  imageCacheHeight: 500,
                  imageCacheWidth: 500,
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
