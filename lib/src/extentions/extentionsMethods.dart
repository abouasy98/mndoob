import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

extension ContextExtention on BuildContext {
  double get height =>
      MediaQuery.of(this).size.height -
      AppBar().preferredSize.height -
      MediaQuery.of(this).padding.top;
  double get width => MediaQuery.of(this).size.width;
}

extension StringExtentsions on String {
  int get wordsCount =>
      trim().isEmpty ? 0 : trim().split(RegExp('(s|\\W)+')).length;
}

extension NumExtentsions<T extends num> on T {
  num addTen() => this + 10;
}
