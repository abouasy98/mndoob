import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AppBottomBar extends StatefulWidget {
  final Function onTap;
  final String userType;
  int inx;

  AppBottomBar({Key key, this.onTap, this.userType, this.inx})
      : super(key: key);
  @override
  _AppBottomBarState createState() => _AppBottomBarState();
}

class _AppBottomBarState extends State<AppBottomBar> {
  int inxShop = 1;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(bottom: 0.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40), topRight: Radius.circular(40))),
      child: BottomNavigationBar(
        showSelectedLabels: false,
        selectedLabelStyle: TextStyle(
          fontSize: 10,
        ),
        unselectedLabelStyle: TextStyle(fontSize: 10, color: Colors.grey),
        unselectedIconTheme: IconThemeData(color: Colors.grey),
        unselectedItemColor: Colors.grey,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        currentIndex: widget.userType != 'user' ? inxShop : widget.inx,
        onTap: (index) {
          setState(() {
            widget.inx = index;
            inxShop = index;
          });
          widget.onTap(index);
        },
        items: widget.userType == 'user'
            ? [
                BottomNavigationBarItem(
                  label: "الاشعارات",
                  activeIcon: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color(0xffFEBF80),
                          Color(0xffFDB883),
                          Color(0xffFE8E90),
                          Color(0xffFE8E90),
                        ],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ImageIcon(
                        AssetImage("assets/icons/notification.png"),
                        size: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage("assets/icons/notification.png"),
                      size: 20,
                    ),
                  ),
                ),
                BottomNavigationBarItem(
                  label: "طلباتي",
                  activeIcon: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color(0xffFEBF80),
                          Color(0xffFDB883),
                          Color(0xffFE8E90),
                          Color(0xffFE8E90),
                        ],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ImageIcon(
                        AssetImage(
                          "assets/icons/checklist1.png",
                        ),
                        size: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage(
                        "assets/icons/checklist1.png",
                      ),
                      size: 20,
                    ),
                  ),
                ),
                BottomNavigationBarItem(
                  label: "حسابي",
                  activeIcon: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color(0xffFEBF80),
                          Color(0xffFDB883),
                          Color(0xffFE8E90),
                          Color(0xffFE8E90),
                        ],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ImageIcon(
                        AssetImage("assets/icons/user.png"),
                        size: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage("assets/icons/user.png"),
                      size: 20,
                      color: Colors.black,
                    ),
                  ),
                ),
                BottomNavigationBarItem(
                  label: "الرئيسية",
                  activeIcon: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color(0xffFEBF80),
                          Color(0xffFDB883),
                          Color(0xffFE8E90),
                          Color(0xffFE8E90),
                        ],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ImageIcon(
                        AssetImage("assets/icons/home.png"),
                        size: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage("assets/icons/home.png"),
                      size: 20,
                    ),
                  ),
                ),
              ]
            : [
                BottomNavigationBarItem(
                  label: "الاشعارات",
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage("assets/icons/alarm.png"),
                      size: 20,
                    ),
                  ),
                ),
                BottomNavigationBarItem(
                  label: "الرئيسية",
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage("assets/icons/home.png"),
                      size: 20,
                    ),
                  ),
                ),
                BottomNavigationBarItem(
                  label: "حسابي",
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ImageIcon(
                      AssetImage("assets/icons/user.png"),
                      size: 20,
                    ),
                  ),
                ),
              ],
      ),
    );
  }
}
