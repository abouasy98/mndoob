import 'package:flutter/material.dart';

class ImageBG2 extends StatelessWidget {
  final double height;
  ImageBG2({this.height});
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/images/background.png',
      fit: BoxFit.cover,
      width: double.infinity,
      height: height,
    );
  }
}
