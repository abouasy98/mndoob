import 'package:flutter/material.dart';

class CustomIcon extends StatelessWidget {
  final String photo;
  final String name;
  final List<Color> colors;
  final Function func;
  CustomIcon({this.photo, this.colors, this.name, this.func});
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return InkWell(
      onTap: func,
      child: Column(
        children: [
          Container(
            width: mediaQuery.width * 0.2,
            height: mediaQuery.height * 0.08,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: colors),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ImageIcon(
                AssetImage(
                  photo,
                ),
                size: 20,
                color: Colors.white,
              ),
            ),
          ),
          Text(
            name,
            style: TextStyle(
                color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
